import React, { Component } from "react";

import Landing from "./home/Landing";
import Life from "./home/Life";

class Home extends Component {
  render() {
    return (
      <div className="home">
        <Landing />
        <Life />
      </div>
    );
  }
}

export default Home;
