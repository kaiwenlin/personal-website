import React, { Component } from "react";
import { Container, Divider } from "semantic-ui-react";

import LifeCards from "./life/LifeCards";

class Life extends Component {
  render() {
    return (
      <section className="Life">
        {/* explicit height specification for IE11 */}
        <Container style={{ height: "100%" }}>
          <Divider horizontal>
            <h1 style={{ textAlign: "center" }}>My Life Till Now</h1>
          </Divider>
          <LifeCards />
        </Container>
      </section>
    );
  }
}

export default Life;
