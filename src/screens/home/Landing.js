import React, { Component } from "react";

import landingImage from "./landing/landing.jpg";
import "./landing/landing.css";

class Landing extends Component {
  render() {
    const titles = ["Innovator", "Consultant", "Engineer"];

    let titleArea = titles.map((title, i) => (
      <span key={i}>
        <span className="title">{title}</span>{" "}
        {i === titles.length - 1 ? "" : " | "}
      </span>
    ));

    return (
      <section className="hero">
        <div
          className="background-image"
          style={{ backgroundImage: `url(${landingImage})` }}
        />
        <div className="hero-content-area">
          <h1 className="introduction">
            Hey, I'm <span className="name">Kaiwen Lin</span>
          </h1>
          <h3 className="titles">{titleArea}</h3>
        </div>
      </section>
    );
  }
}

export default Landing;
