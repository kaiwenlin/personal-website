import React, { Component } from "react";
import { Item } from "semantic-ui-react";

import LifeCard from "./lifeCards/LifeCard";
import "./lifeCards/LifeCards.css";

import { makeCloudinarySrc } from "../../../utils/cloudinary";

const IMG_DIR = "/work-time/";

class LifeCards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lifeEvents: [
        {
          title: "Futurice",
          img: {
            src: makeCloudinarySrc(IMG_DIR + "futurice_logo.png")
          },
          info:
            "Helping clients unleash innovation through agile software development as a full stack web developer. Developing new valuable features through lean service creation to make the biggest impact in the shortest amount of time.",
          position: "Software Developer",
          startDate: "Dec 2018",
          location: { city: "Stockholm", country: "Sweden" }
        },
        {
          title: "PA Consulting Group",
          img: {
            src: makeCloudinarySrc(IMG_DIR + "pa_logo.png")
          },
          info:
            "Here, I help clients innovate through developing new products and solutions. I've been developing scalable back-end API's and responsive front-end webpages to interact and control test equipment for mass consumer products. I've been developing firmware for said mass consumer products as well as designing and creating  data visualisation tools surrounding the system. The variety of work I've been able to do here is amazing and it has definitely shaped and focused my interests for the future.",
          position: "Analyst | Electronics & Software Engineer",
          startDate: "Oct 2017",
          endDate: "Nov 2018",
          location: { city: "Cambridge", country: "UK" }
        },
        {
          title: "Durham University",
          img: {
            src: makeCloudinarySrc(IMG_DIR + "durham_logo.png")
          },
          info: (
            <div>
              <p>
                Durham has been the pillar to both my knowledge and hobbies. The
                years I spent here were full of diverse opportunities,
                aspirations and goals. The multidisciplinary nature of my degree
                challenged me in all aspects engineering and entrepreneuring -
                everything from digital electronics to how to build a startup.
                The breadth of this course solidified my core problem solving
                ability regardless of discipline. My third year abroad at the
                National University of Singapore strengthened my ability to
                adapt while pushing me to expand, challenge and adjust my world
                views.
              </p>
              <p>
                My experience dancing and training with{" "}
                <a
                  href="https://www.facebook.com/groups/durhambreakdance/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  Chili Con Crew
                </a>{" "}
                and{" "}
                <a
                  href="https://www.instagram.com/breakinus/?hl=en"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  BreakiNUS
                </a>{" "}
                showed me the power of expression. Training with both crews
                pushed and expanded my comfort zone while reinforcing the
                concept of work hard play hard.
              </p>
            </div>
          ),
          position: "Masters of Engineering",
          startDate: "Oct 2013",
          endDate: "Jun 2017",
          location: { city: "Durham", country: "UK" }
        },
        {
          title: "PA Consulting Group",
          img: {
            src: makeCloudinarySrc(IMG_DIR + "pa_intern_logo.jpg")
          },
          info:
            "I spent this summer learning about the world of consulting as well as designing and developing a demonstrator platform for an all-weather navigation system to be used in self driving vehicles. I was honored to work aside the brightest minds and was inspired by all the capabilities within one building. This summer showed me my place in a tech-driven world and boosted my enthusiasm and motivation to work in technology.",
          position: "Summer Intern",
          startDate: "Jun 2015",
          endDate: "Sept 2015",
          location: { city: "Cambridge", country: "UK" }
        },
        {
          title: "MyTutor",
          img: {
            src: makeCloudinarySrc(IMG_DIR + "mytutor_logo.png")
          },
          info:
            "I used my ability to easily convey complex concepts and constructs to high school maths and physics students. I was able to help several students achieve remarkable results through encouragement, calmness and patience. Being a tutor taught me the importance of being able to articulate ideas clearly - and a big part of this was understanding how to put yourself in the shoes of another.",
          position: "Personal Maths and Physics Tutor",
          startDate: "Nov 2014",
          endDate: "Jun 2015",
          location: { city: "Durham", country: "UK" }
        }
      ]
    };
  }

  // TODO: Add get method to API to populate lifeEvents

  render() {
    const { lifeEvents } = this.state;

    return (
      <div className="LifeCards">
        {lifeEvents ? (
          <Item.Group relaxed="very">
            {lifeEvents.map((lifeEvent, i) => (
              <LifeCard lifeEvent={lifeEvent} key={i} />
            ))}
          </Item.Group>
        ) : null}
      </div>
    );
  }
}

export default LifeCards;
