import React from "react";
import PropTypes from "prop-types";
import { Item } from "semantic-ui-react";

import "./lifeCard/LifeCard.css";

const LifeCard = props => {
  let {
    img,
    title,
    info,
    position,
    startDate,
    endDate,
    location
  } = props.lifeEvent;

  return (
    <Item className="LifeCard" id="LifeCard">
      <Item.Image size="medium" src={img.src} srcSet={img.srcSet} />
      <Item.Content style={{ width: "100%" }}>
        <Item.Header>{title}</Item.Header>
        <Item.Meta>
          <div className="position">
            {position}, {startDate} {endDate ? ` - ${endDate}` : null}
          </div>
          {location ? (
            <div className="location" style={{ paddingTop: "5px" }}>
              {location.city}, {location.country}
            </div>
          ) : null}
        </Item.Meta>
        <Item.Description>{info}</Item.Description>
      </Item.Content>
    </Item>
  );
};

LifeCard.propTypes = {
  lifeEvent: PropTypes.shape({
    img: PropTypes.shape({
      src: PropTypes.string.isRequired,
      srcSet: PropTypes.arrayOf(PropTypes.string)
    }).isRequired,
    title: PropTypes.string.isRequired,
    position: PropTypes.string.isRequired,
    startDate: PropTypes.string.isRequired,
    endDate: PropTypes.string,
    location: PropTypes.shape({
      city: PropTypes.string.isRequired,
      country: PropTypes.string.isRequired
    })
  }).isRequired
};

export default LifeCard;
