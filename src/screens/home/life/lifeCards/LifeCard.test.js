import React from "react";
import ReactDOM from "react-dom";
import LifeCard from "./LifeCard";

it("renders all without crashing", () => {
  const div = document.createElement("div");

  const dummyLifeEvent = {
    img: "fake_url.com",
    title: "Lorem Ipsum",
    info: "deadbeef",
    position: "Lorem Ipsum",
    startDate: "Nov 2010",
    endDate: "Feb 2020",
    location: {
      city: "Mars",
      country: "Earth"
    }
  };

  ReactDOM.render(<LifeCard lifeEvent={dummyLifeEvent} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders minimum without crashing", () => {
  const div = document.createElement("div");

  const dummyLifeEvent = {
    img: "fake_url.com",
    title: "Lorem Ipsum",
    info: "deadbeef",
    position: "Lorem Ipsum",
    startDate: "Nov 2010"
  };

  ReactDOM.render(<LifeCard lifeEvent={dummyLifeEvent} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
