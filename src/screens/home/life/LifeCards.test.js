import React from "react";
import ReactDOM from "react-dom";
import LifeCards from "./LifeCards";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<LifeCards />, div);
  ReactDOM.unmountComponentAtNode(div);
});
