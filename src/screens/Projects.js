import React, { Component } from "react";
import { Container, Divider } from "semantic-ui-react";

import Header from "../components/Header";
import ProjectList from "./projects/ProjectList";

class Projects extends Component {
  render() {
    return (
      <div className="Projects">
        <Header title="Projects" />
        <section>
          {/* explicit height specification for IE11 */}
          <Container style={{ height: "100%" }}>
            <Divider horizontal>
              <h1>I Like to Make Stuff</h1>
            </Divider>
            <p style={{ textAlign: "center" }}>
              Here's a collection of projects, small and large, of which I've
              designed and built over the past few years.
            </p>
            <ProjectList />
          </Container>
        </section>
      </div>
    );
  }
}

export default Projects;
