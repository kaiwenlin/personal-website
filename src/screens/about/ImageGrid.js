import React, { Component } from "react";
import Gallery from "react-photo-gallery";
import Lightbox from "react-images";

import {
  makeCloudinarySrc,
  makeCloudinarySrcSet
} from "../../utils/cloudinary";

const IMG_DIR = "/free-time/";

const DEFAULT_IMAGES = [
  { id: "lions_rock.jpg", width: 4000, height: 3000 },
  { id: "IMG_20180101_175207.jpg", width: 3968, height: 2976 },
  { id: "IMG_20180413_104611.jpg", width: 3968, height: 2976 },
  { id: "IMG_20161002_090110.jpg", width: 3826, height: 2976 },
  // { id: "IMG_20170621_182514.jpg", width: 3968, height: 2976 },
  { id: "IMG_20170703_230521_1.jpg", width: 2976, height: 3968 },
  { id: "k_bbq.jpg", width: 1200, height: 1600 },
  { id: "20170710_143748_0.jpg", width: 3456, height: 4608 },
  { id: "yunnan.jpg", width: 6944, height: 1744 },
  { id: "IMG-20180419-WA0036.jpg", width: 1066, height: 1600 },
  // { id: "IMG_20160917_184331.jpg", width: 3968, height: 2976 },
  { id: "IMG_20180428_192512.jpg", width: 3968, height: 2976 },
  { id: "IMG-20180416-WA0027.jpg", width: 1600, height: 1066 },
  { id: "font.png", width: 1140, height: 855 },
  // { id: "IMG-20180412-WA0015.jpg", width: 898, height: 1600 },
  { id: "IMG-20180107-WA0009.jpg", width: 900, height: 1600 },
  { id: "too_chill.jpg", width: 1600, height: 1066 }
];

const photos = DEFAULT_IMAGES.map(({ id, width, height }) => ({
  src: makeCloudinarySrc(IMG_DIR + id),
  srcSet: [
    `${makeCloudinarySrcSet(IMG_DIR + id, 1600)} 1600w`,
    `${makeCloudinarySrcSet(IMG_DIR + id, 1024)} 1024w`,
    `${makeCloudinarySrcSet(IMG_DIR + id, 800)} 800w`,
    `${makeCloudinarySrcSet(IMG_DIR + id, 500)} 500w`,
    `${makeCloudinarySrcSet(IMG_DIR + id, 320)} 320w`
  ],
  sizes: ["(min-width: 480px) 50vw,(min-width: 1024px) 33.3vw,100vw"],
  width,
  height
}));

class ImageGrid extends Component {
  constructor() {
    super();

    this.state = { currentImage: 0 };
    this.closeLightbox = this.closeLightbox.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
  }
  openLightbox(event, obj) {
    this.setState({
      currentImage: obj.index,
      lightboxIsOpen: true
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1
    });
  }

  render() {
    return (
      <div style={{ width: "100%" }}>
        {/* explicit width specification for IE11 */}
        <Gallery photos={photos} onClick={this.openLightbox} columns={5} />
        <Lightbox
          images={photos}
          onClose={this.closeLightbox}
          onClickPrev={this.gotoPrevious}
          onClickNext={this.gotoNext}
          currentImage={this.state.currentImage}
          isOpen={this.state.lightboxIsOpen}
        />
      </div>
    );
  }
}

export default ImageGrid;
