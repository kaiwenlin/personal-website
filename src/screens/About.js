import React, { Component } from "react";
import { Container, Divider } from "semantic-ui-react";

import Header from "../components/Header";
import ImageGrid from "./about/ImageGrid";

class About extends Component {
  render() {
    return (
      <div className="About">
        <Header title="About" />
        <section>
          <Container>
            <Divider horizontal style={{ paddingBottom: "40px" }}>
              <h1>My Story</h1>
            </Divider>
            <p style={{ textAlign: "center" }}>
              I'm an enthusiastic individual who loves to learn. New knowledge
              fuels my motivation and enthusiasm to design, make and create. I
              strive for interesting and intellectually stimulating
              conversations and like to see how far I can push my ability to
              make and create things. My current particular interests lie in
              Digitilization and Web Development (in particular full-stack
              design using ReactJS)
            </p>
            <p style={{ textAlign: "center" }}>
              I'm British-Born and have spent the majority of my life in the UK,
              with the odd years living in Shanghai and Singapore. My
              multicultural upbringing has shaped who I am today. I'm currently
              based in Stockholm, Sweden but am open to move to anywhere in the
              world. I love to travel to see, feel and soak in new experiences.
            </p>
          </Container>
        </section>
        <section>
          {/* explicit height specification for IE11 */}
          <Container style={{ height: "100%" }}>
            <Divider horizontal style={{ paddingBottom: "40px" }}>
              <h1>What I Get Up To</h1>
            </Divider>
          </Container>
          <ImageGrid />
        </section>
      </div>
    );
  }
}

export default About;
