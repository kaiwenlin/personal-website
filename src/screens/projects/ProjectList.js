import React, { Component } from "react";
import { Item } from "semantic-ui-react";

import ProjectCard from "./projectList/ProjectCard";
import "./projectList/ProjectList.css";

import { makeCloudinarySrc } from "../../utils/cloudinary";

const IMG_DIR = "/work-time/";

class ProjectList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      projectDetails: [
        {
          title: "Personal Website",
          img: makeCloudinarySrc(IMG_DIR + "personal_website.jpg"),
          info: (
            <div>
              I wanted to set myself a challenge - create a personal website in
              a day. I did it in four! This site was created using ReactJS and
              NodeJS - with the source code available
              <a
                href="https://gitlab.com/kaiwenlin/personal-website"
                rel="noopener noreferrer"
                target="_blank"
              >
                {" "}
                here
              </a>
              . This journey taught me a lot in terms of front-end design and
              project structure architecture. I tried to use a fractal based
              architecture so that I can scale the site in the future (including
              the Express and Mongoose back-end once done).
            </div>
          ),
          startDate: "Aug 2018"
        },
        {
          title: "The Useless Box",
          img: makeCloudinarySrc(IMG_DIR + "IMG_20180806_001612.jpg"),
          info: <div>I saw this on the internet. I wanted one. I made it.</div>,
          startDate: "Apr 2018"
        },
        {
          title: "Bilingual Technical Consultancy Website",
          img: makeCloudinarySrc(IMG_DIR + "skn_technology.png"),
          info:
            "Wanting to see the power of WordPress - I made this website using their ecosystem. What better way to feel the power than by using it? The many features of WordPress made making this website a breeze. I was able to easily implement this simple design by using themes and easily accessible add-ons. WordPress was great to get this site up and running, but I would like to remake this using ReactJS so that I have more fine-tuned control over the design and implementation and not worry about a third-party add-on breaking a feature of the site.",
          startDate: "Nov 2017"
        },
        {
          title:
            "Omnidirectional Stereo Vision for Environment Mapping in Driverless Vehicles",
          img: makeCloudinarySrc(IMG_DIR + "system.jpg"),
          info:
            "One of the largest projects I have done - also my Master's Thesis / Final Year Project at Durham University. I designed, implemented and showed that a stereo vision system could be built by using two commercial 360 degree cameras (e.g. Ricoh Theta S). Prior to this project I had zero computer vision knowledge and this project gave me a crash course in computer vision while at the same time allowing me to do this state-of-the-art research. This paper was published in the International Conference of Image Analysis and Recognition (ICIAR 2018). This whole experience gave me the confidence to take on any project, no matter how big, and be confident that I'll be able to find a solution.",
          startDate: "Oct 2016"
        },
        {
          title: "Chili Con Crew Website",
          img: makeCloudinarySrc(IMG_DIR + "chili_con_crew.jpg"),
          info: (
            <div>
              Just a simple website I built for the Durham University Breakdance
              Society (which are informally known as Chili Con Crew). I was
              curious of what I could build with only HTML and CSS, the answer
              was{" "}
              <a
                href="http://community.dur.ac.uk/break.dance/"
                rel="noopener noreferrer"
                target="_blank"
              >
                {" "}
                this
              </a>
              . As this was mainly targeted at university students, it was
              critical that I made the site responsive, clean and stand out
              compared to other societies. From this, I learnt what Search
              Engine Optimization (SEO) was and the importance it had on
              websites.
            </div>
          ),
          startDate: "Oct 2016"
        }
      ]
    };
  }

  // TODO: Add get method to API to populate lifeEvents

  render() {
    const { projectDetails } = this.state;

    return (
      <div className="ProjectList">
        {projectDetails ? (
          <Item.Group relaxed="very">
            {projectDetails.map((projectDetail, i) => (
              <ProjectCard projectDetail={projectDetail} key={i} />
            ))}
          </Item.Group>
        ) : null}
      </div>
    );
  }
}

export default ProjectList;
