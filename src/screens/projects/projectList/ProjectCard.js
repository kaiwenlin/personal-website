import React from "react";
import PropTypes from "prop-types";
import { Item } from "semantic-ui-react";

import "./projectCard/ProjectCard.css";

const ProjectCard = props => {
  let { img, title, info, startDate, endDate } = props.projectDetail;

  return (
    <Item className="ProjectCard" id="ProjectCard">
      <Item.Image size="medium" src={img} />
      <Item.Content style={{ width: "100%" }}>
        <Item.Header>{title}</Item.Header>
        <Item.Meta>
          <div className="date">
            {startDate} {endDate ? ` - ${endDate}` : null}
          </div>
        </Item.Meta>
        <Item.Description>{info}</Item.Description>
      </Item.Content>
    </Item>
  );
};

ProjectCard.propTypes = {
  projectDetail: PropTypes.shape({
    img: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    startDate: PropTypes.string.isRequired,
    endDate: PropTypes.string
  }).isRequired
};

export default ProjectCard;
