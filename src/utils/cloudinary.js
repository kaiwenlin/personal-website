export function makeCloudinarySrc(id) {
  return `https://res.cloudinary.com/kaiwenlin/image/upload/personal-website/${id}`;
}

export function makeCloudinarySrcSet(id, width) {
  return `https://res.cloudinary.com/kaiwenlin/image/upload/w_${width}/personal-website/${id}`;
}
