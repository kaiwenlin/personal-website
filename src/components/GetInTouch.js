import React, { Component } from "react";
import { Divider, Container } from "semantic-ui-react";

import WeChatModal from "./getInTouch/WeChatModal";
import "./getInTouch/GetInTouch.css";

class GetInTouch extends Component {
  constructor() {
    super();

    this.state = {
      iconSize: "9x"
    };
  }

  calculateIconSize() {
    let window_width = window.innerWidth;
    if (window_width < 490) {
      this.setState({
        iconSize: "5x"
      });
    } else {
      this.setState({
        iconSize: "9x"
      });
    }
  }

  componentDidMount() {
    this.calculateIconSize();
    window.addEventListener("resize", () => {
      this.calculateIconSize();
    });
  }

  componentWillUnmount() {
    window.removeEventListener("resize");
  }

  render() {
    const { iconSize } = this.state;

    return (
      <section className="GetInTouch">
        <Container>
          <Divider horizontal style={{ paddingBottom: "40px" }}>
            <h1 style={{ textAlign: "center" }}>Get In Touch</h1>
          </Divider>
          <p style={{ textAlign: "center" }}>
            Need a website built? Got a cool idea for a new project and you'd
            like a hand? Or just want to connect?
          </p>
          <p style={{ textAlign: "center" }}>
            Come give me a shout! I'd love to help out!
          </p>
        </Container>
        <div className="social-media-icons" style={{ marginTop: "30px" }}>
          <a
            href="https://www.linkedin.com/in/kaiwenlin1995/"
            rel="noopener noreferrer"
            target="_blank"
          >
            <i className={`fab fa-linkedin fa-${iconSize}`} />
          </a>
          <WeChatModal iconSize={iconSize} />
          <a href="mailto:kaiwen@kaiwenlin.com">
            <i className={`fas fa-envelope fa-${iconSize}`} />
          </a>
        </div>
      </section>
    );
  }
}

export default GetInTouch;
