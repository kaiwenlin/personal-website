import React, { Component } from "react";
import { Menu } from "semantic-ui-react";
import { Link, withRouter } from "react-router-dom";
import { animateScroll as scroll } from "react-scroll";

class Navbar extends Component {
  state = { activeItem: "/" };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.location.pathname !== prevState.activeItem) {
      return {
        ...prevState,
        activeItem: nextProps.location.pathname
      };
    } else {
      return {
        ...prevState
      };
    }
  }

  handleItemClick = (e, { name }) => {
    if (name === "/contact") {
      scroll.scrollToBottom({
        smooth: true
      });
    } else {
      this.setState({ activeItem: name });
    }
  };

  render() {
    const { activeItem } = this.state;

    return (
      <header className="Navbar">
        <Menu secondary pointing>
          <Menu.Item
            as={Link}
            name="home"
            active={activeItem === "/"}
            onClick={this.handleItemClick}
            to="/"
          />
          <Menu.Menu position="right">
            <Menu.Item
              as={Link}
              name="/projects"
              active={activeItem === "/projects"}
              onClick={this.handleItemClick}
              to="/projects"
            />
            <Menu.Item
              as={Link}
              name="/about"
              active={activeItem === "/about"}
              onClick={this.handleItemClick}
              to="/about"
            />
            <Menu.Item name="/contact" onClick={this.handleItemClick} />
          </Menu.Menu>
        </Menu>
      </header>
    );
  }
}

export default withRouter(Navbar);
