import React, { Component } from "react";
import PropTypes from "prop-types";

import "./header/Header.css";

class Header extends Component {
  render() {
    const { img, title } = this.props;

    return (
      <section className="Header">
        <div
          className="background-image"
          style={{ backgroundImage: `url(${img})` }}
        />
        <div className="header-content-area">
          <h1>{title}</h1>
        </div>
      </section>
    );
  }
}

Header.propTypes = {
  img: PropTypes.string,
  title: PropTypes.string.isRequired
};

export default Header;
