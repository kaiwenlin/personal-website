import React from "react";
import ReactDOM from "react-dom";
import GetInTouch from "./GetInTouch";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<GetInTouch />, div);
  ReactDOM.unmountComponentAtNode(div);
});
