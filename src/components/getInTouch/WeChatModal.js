import React from "react";
import { Modal, Image, Header } from "semantic-ui-react";

import WeChatQR from "./weChatModal/wechat_code.png";

const WeChatModal = props => {
  return (
    <Modal
      trigger={
        <i
          className={`fab fa-weixin fa-${props.iconSize}`}
          style={{ cursor: "pointer" }}
        />
      }
      style={{ width: "auto" }}
    >
      <Modal.Header style={{ textAlign: "center" }}>
        Scan to Add Me!
      </Modal.Header>
      <Modal.Content
        image
        style={{ flexDirection: "column", justifyContent: "center" }}
      >
        <Image
          wrapped
          size="medium"
          src={WeChatQR}
          style={{ alignSelf: "center", height: "100%" }}
        />
        <Modal.Description style={{ paddingTop: "20px", paddingLeft: "0" }}>
          <Header style={{ textAlign: "center" }}>WeChat ID: kaiwen599</Header>
        </Modal.Description>
      </Modal.Content>
    </Modal>
  );
};

export default WeChatModal;
