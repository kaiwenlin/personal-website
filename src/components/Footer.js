import React, { Component } from "react";
import Moment from "react-moment";

class Footer extends Component {
  render() {
    return (
      <section className="Footer">
        <p>Designed and Hand Built by Kaiwen Lin</p>
        <small>
          Copyright &copy; <Moment format="YYYY">{Date.now()}</Moment> Kaiwen
          Lin
        </small>
      </section>
    );
  }
}

export default Footer;
