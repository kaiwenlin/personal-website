import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";

import Navbar from "./components/Navbar";
import Home from "./screens/Home";
import Projects from "./screens/Projects";
import About from "./screens/About";
import GetInTouch from "./components/GetInTouch";
import Footer from "./components/Footer";

import "./App.css";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/projects" component={Projects} />
            <Route exact path="/about" component={About} />
            <Redirect to="/" />
          </Switch>
          <GetInTouch />
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
